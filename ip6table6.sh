#!/bin/bash

# Limpa as regras existentes
ip6tables -F
ip6tables -X
ip6tables -Z

# Define a política padrão como DROP para INPUT, FORWARD e OUTPUT
ip6tables -P INPUT DROP
ip6tables -P FORWARD DROP
ip6tables -P OUTPUT DROP

# Permitir loopback (localhost)
ip6tables -A INPUT -i lo -j ACCEPT
ip6tables -A OUTPUT -o lo -j ACCEPT

# Permitir tráfego estabelecido e relacionado
ip6tables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
ip6tables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
ip6tables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

# Permitir tráfego ipv6-icmp (ping)
ip6tables -A INPUT -p ipv6-icmp -j ACCEPT
ip6tables -A OUTPUT -p ipv6-icmp -j ACCEPT

# Permitir tráfego SSH
ip6tables -A INPUT -p tcp --dport 22 -j ACCEPT

# Permitir tráfego HTTP e HTTPS
ip6tables -A INPUT -p tcp -m multiport --dports 80,443 -j ACCEPT

# Permitir tráfego FTP
ip6tables -A INPUT -p tcp -m multiport --dports 20,21 -j ACCEPT

# Permitir tráfego SMTP (portas 25, 465, 587)
ip6tables -A INPUT -p tcp -m multiport --dports 25,465,587 -j ACCEPT

# Permitir tráfego POP3 (portas 110, 995)
ip6tables -A INPUT -p tcp -m multiport --dports 110,995 -j ACCEPT

# Permitir tráfego IMAP (portas 143, 993)
ip6tables -A INPUT -p tcp -m multiport --dports 143,993 -j ACCEPT

# Permitir tráfego para as outras portas TCP
ip6tables -A INPUT -p tcp -m multiport --dports 8090,40110:40210 -j ACCEPT

# Permitir tráfego DNS (portas 53, 5353)
ip6tables -A INPUT -p udp -m multiport --dports 53,443 -j ACCEPT

# Permitir tráfego NTP (portas 123)
ip6tables -A INPUT -p udp --dport 123 -j ACCEPT

# Permitir tráfego para BADGUY
ip6tables -A INPUT -s 10.0.0.1 -j DROP

# Adicionar regras de mangle
ip6tables -t mangle -A PREROUTING -p tcp --dport 80 -j TOS --set-tos 0x10
ip6tables -t mangle -A PREROUTING -p tcp --dport 443 -j TOS --set-tos 0x18
ip6tables -t mangle -A PREROUTING -p tcp --dport 22 -j TOS --set-tos 0x08

# Configuração de mangle para DDoS
ip6tables -A INPUT -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP
ip6tables -A INPUT -p tcp ! --syn -m conntrack --ctstate NEW -j DROP
ip6tables -A INPUT -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP
ip6tables -A INPUT -p tcp --tcp-flags FIN,SYN FIN,SYN -j DROP
ip6tables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
ip6tables -A INPUT -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
ip6tables -A INPUT -p tcp --tcp-flags FIN,ACK FIN -j DROP
ip6tables -A INPUT -p tcp --tcp-flags ACK,URG URG -j DROP

# Adicionar regras de DDoS
ip6tables -N syn-flood
ip6tables -A INPUT -p tcp --syn -j syn-flood
ip6tables -A syn-flood -m limit --limit 1/s --limit-burst 4 -j RETURN
ip6tables -A syn-flood -j DROP
ip6tables -A INPUT -p ipv6-icmp --ipv6-icmpv6-type echo-request -m limit --limit 1/s --limit-burst  4 -j ACCEPT
ip6tables -A INPUT -p ipv6-icmp --ipv6-icmpv6-type echo-request -j DROP
ip6tables -A INPUT -p ipv6-icmp --ipv6-icmpv6-type echo-reply -m limit --limit 1/s --limit-burst 4 -j ACCEPT
ip6tables -A INPUT -p ipv6-icmp --ipv6-icmpv6-type echo-reply -j DROP
ip6tables -A INPUT -p ipv6-icmp --ipv6-icmpv6-type destination-unreachable -m limit --limit 1/s --limit-burst 4 -j ACCEPT
ip6tables -A INPUT -p ipv6-icmp --ipv6-icmpv6-type destination-unreachable -j DROP
ip6tables -A INPUT -p ipv6-icmp --ipv6-icmpv6-type time-exceeded -m limit --limit 1/s --limit-burst 4 -j ACCEPT
ip6tables -A INPUT -p ipv6-icmp --ipv6-icmpv6-type time-exceeded -j DROP

# Permitir ping
ip6tables -A INPUT -p ipv6-ipv6-icmp --ipv6-icmpv6-type echo-request -j DROP
ip6tables -A INPUT -p ipv6-ipv6-icmp --ipv6-icmpv6-type echo-reply -m limit --limit 1/s --limit-burst 4 -j ACCEPT
ip6tables -A INPUT -p ipv6-ipv6-icmp --ipv6-icmpv6-type echo-reply -j DROP
ip6tables -A INPUT -p ipv6-ipv6-icmp --ipv6-icmpv6-type destination-unreachable -m limit --limit 1/s --limit-burst 4 -j ACCEPT
ip6tables -A INPUT -p ipv6-ipv6-icmp --ipv6-icmpv6-type destination-unreachable -j DROP
ip6tables -A INPUT -p ipv6-ipv6-icmp --ipv6-icmpv6-type time-exceeded -m limit --limit 1/s --limit-burst 4 -j ACCEPT
ip6tables -A INPUT -p ipv6-ipv6-icmp --ipv6-icmpv6-type time-exceeded -j DROP

#Salva as regras
ip6tables-save > /etc/sysconfig/ip6tables
