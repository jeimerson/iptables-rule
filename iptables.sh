#!/bin/bash

# Limpa as regras existentes
iptables -F
iptables -X
iptables -Z

# Define a política padrão como DROP para INPUT, FORWARD e OUTPUT
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP

# Permitir loopback (localhost)
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Permitir tráfego estabelecido e relacionado
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

# Permitir tráfego ICMP (ping)
iptables -A INPUT -p icmp -j ACCEPT
iptables -A OUTPUT -p icmp -j ACCEPT

# Permitir tráfego SSH
iptables -A INPUT -p tcp --dport 22 -j ACCEPT

# Permitir tráfego HTTP e HTTPS
iptables -A INPUT -p tcp -m multiport --dports 80,443 -j ACCEPT

# Permitir tráfego FTP
iptables -A INPUT -p tcp -m multiport --dports 20,21 -j ACCEPT

# Permitir tráfego SMTP (portas 25, 465, 587)
iptables -A INPUT -p tcp -m multiport --dports 25,465,587 -j ACCEPT

# Permitir tráfego POP3 (portas 110, 995)
iptables -A INPUT -p tcp -m multiport --dports 110,995 -j ACCEPT

# Permitir tráfego IMAP (portas 143, 993)
iptables -A INPUT -p tcp -m multiport --dports 143,993 -j ACCEPT

# Permitir tráfego para as outras portas TCP
iptables -A INPUT -p tcp -m multiport --dports 8090,40110:40210 -j ACCEPT

# Permitir tráfego DNS (portas 53, 5353)
iptables -A INPUT -p udp -m multiport --dports 53,443 -j ACCEPT

# Permitir tráfego NTP (portas 123)
iptables -A INPUT -p udp --dport 123 -j ACCEPT

# Permitir tráfego para BADGUY
iptables -A INPUT -s 10.0.0.1 -j DROP

# Adicionar regras de mangle
iptables -t mangle -A PREROUTING -p tcp --dport 80 -j TOS --set-tos 0x10
iptables -t mangle -A PREROUTING -p tcp --dport 443 -j TOS --set-tos 0x18
iptables -t mangle -A PREROUTING -p tcp --dport 22 -j TOS --set-tos 0x08

# Adicionar regras de DDoS
iptables -N syn-flood
iptables -A INPUT -p tcp --syn -j syn-flood
iptables -A syn-flood -m limit --limit 1/s --limit-burst 4 -j RETURN
iptables -A syn-flood -j DROP
iptables -A INPUT -p icmp --icmp-type echo-request -m limit --limit 1/s --limit-burst  4 -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-request -j DROP
iptables -A INPUT -p icmp --icmp-type echo-reply -m limit --limit 1/s --limit-burst 4 -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-reply -j DROP
iptables -A INPUT -p icmp --icmp-type destination-unreachable -m limit --limit 1/s --limit-burst 4 -j ACCEPT
iptables -A INPUT -p icmp --icmp-type destination-unreachable -j DROP
iptables -A INPUT -p icmp --icmp-type time-exceeded -m limit --limit 1/s --limit-burst 4 -j ACCEPT
iptables -A INPUT -p icmp --icmp-type time-exceeded -j DROP

#Salva as regras
iptables-save > /etc/sysconfig/iptables
